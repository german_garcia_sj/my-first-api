package com.folcademy.myfirstapi.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GodbyeController {
    @PostMapping("/goodbye")
    public String goodbye(){
        return "ADIOS MUNDO";
    }
}
